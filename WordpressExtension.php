<?php namespace Decoupled\Wordpress\Extension;

use Decoupled\Wordpress\Extension\Assets\WPAssetsExtension;
use Decoupled\Wordpress\Event\EventExtension as WPEventExtension;
use Decoupled\Wordpress\Extension\Timber\TimberExtension;
use Decoupled\Wordpress\Process\Assets\AssetProcess;
use Decoupled\Core\Application\ApplicationExtension;
use Decoupled\Core\Application\ApplicationContainer;

class WordpressExtension extends ApplicationExtension{

    public function getName()
    {
        return 'wp.extension';
    }

    public function extend()
    {
        $app = $this->getApp();

        //register base extensions

        $app->uses( 
            new WPAssetsExtension(),
            new WPEventExtension(),
            new TimberExtension()
        );

        //register bundle process
        
        $app->uses(function( $bundleInitializer, $app ){

            $bundleInitializer->uses(
                new AssetProcess($app)
            );
        });

        $this->registerEvents( $app );
    }

    protected function registerEvents( ApplicationContainer $app )
    {
        //initializes the bundles, on $app.run event

        $app->when( '$app.run' )->uses(function( $bundleCollection, $app ){

            $app->dispatch( '$bundle.register' );

            $app->process()->init( $bundleCollection );
        });

        //resolve actions on states on $app.run event
        
        $app->when( '$app.run' )->uses(function( $state, $stateRouter, $actionQueue ){

           $actions = $stateRouter->resolve( $state );

           $actionQueue( $actions );
        });

        //run the application on wordpress template include

        $app->uses(function( $wpEventFactory, $app ){

            $wpEvent = $wpEventFactory;

            $app->when( $wpEvent('template_include') )->uses(function( $event ) use($app){

                $app->dispatch('$app.init');

                list( $template ) = $event->getParameters();

                $app->dispatch( '$app.run', [
                    'state'       => get_body_class('$app'),
                    'template'    => $template
                ]);

                echo $app->output( $app['$action.queue'] )->output();
            });
        });
    }
}